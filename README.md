StaticMeshPlugin
================

Postprocessing of temporal dataset with static geometry is wide spread in
several physics domains like CFD and mechanics.

The aim of this plugin is to optimize post-processing and visualization time
of transient datasets that have static geometries. Currently, in this case
ParaView/VTK filters do not handle those datasets specifically, and thus
recompute everything when the time step changes. However, one can easily think
to a typical case of slicing a static mesh: the slice geometry will not change
over time, only the attached data (point, cell or field data) will change, so
one could avoid the computation time linked to the slice geometry computation.

A static mesh optimized (SMO) implementation of a filter typically uses a cache to store the geometry.
It will then only compute the data and attached it to a shallow copy of the cached geometry. A static filter
can detect if its cache becomes invalid by using the `GetMeshMTime()` method.
In that case, the cache will be recomputed. Of course, this can provide huge
speedups when used right.

This plugin addresses the case of static meshes by replacing certain filters
and readers by their own SMO version that operates with static meshes.

For now, the following filters are supported by this plugin:

* `vtkDataSetSurfaceFilter` / Extract Surface (used internally by ParaView for the rendering)
* `vtkPlaneCutter` / Slice with plane
* `vtkUnstructuredGridGhostCellGenerator` / Generate Ghost Cells (only available if `PARAVIEW_USE_MPI` is **ON**)

Note that for now, only unstructured grids or multiblocks containing a single
unstructured grid are supported in input.

A SMO version of a reader is similar, as it will generally only read
the geometry the first time it is executed, and then only read the data
associated with the following time steps and update it on its cached geometry.

Currently, only a single reader is supported by this plugin:

* EnSight Reader, in all its versions: 6, 6Binary, Gold and GoldBinary.

In the EnSight format, there is no way to detect if a dataset is actually static
or not, so the user should choose by checking, or not, the `UseStaticMesh`
checkbox.

There is also a specific source in the plugin, that let you generate static
meshes from scratch: `vtkTemporalUGWavelet` / Temporal Unstructured Grid Wavelet

Moreover, the specific filter `vtkForceStaticMesh` / Force Static Mesh provided by this plugin forces
its output to be considered as a static mesh and can be used to optimize the
pipeline downstream.

Additional logging may be enabled using the environment variable: `VTK_DEBUG_STATIC_MESH`.

There are a few limitations related to the design of this plugin:

1. Static pipeline break

Most "non-static" filters (e.g. Clip) completely rewrite the geometry of
the mesh when executed, so including one between two static filters/readers
will trigger cache recomputation in the filters below, loosing all potential
speedups. A work around is to add a `ForceStaticMesh` filter to make sure filters
downstream keep using static mesh optimization, considering those filters do not
break the static pipeline.

2. Parallel support

The EnSight reader given in this plugin does not support parallelism.
Even if `PARAVIEW_USE_MPI` is true, the file will be read with a single node.

3. Reader XML duplication

There is now way to inherit and override a proxy at the same time. In order
to provide static related option in the GUI of the readers, it was necessary
to duplicate the XML of these proxies.

4. `VTK_ALL_NEW_OBJECT_FACTORY`

This plugin relies on the VTK factory mechanism in order to replace a
filter by another. This option is not enabled by default in ParaView for
performance reasons. For this reason, this plugin is not compatible with
the binary release of ParaView.

5. Multi-block datasets support

The `vtkStaticPlaneCutter` and `vtkForceStaticMesh` applied on a multiblock dataset only
processes the first block.

Requirements
------------

* ParaView version 5.8 or more

This plugin requires that you compile ParaView. See the build instructions
[here](https://gitlab.kitware.com/paraview/paraview/blob/master/Documentation/dev/build.md).
Also, this plugin requires ParaView to be compiled with the CMake flag:
`VTK_ALL_NEW_OBJECT_FACTORY` set to **ON**.

Once you have built and installed ParaView and this plugin, you can load
it into ParaView using the **Tools > Manage Plugins...** menu. Select:
**Load New...**, navigate the install folder of this plugin and pick the
file: **lib/paraview-5.8/plugins/StaticMesh/StaticMesh.so/dylib/dll**
(the exact location of this file may vary depending on your system
and ParaView version).

Use-cases
---------

Here are some usecase examples to understand how to use this plugin.

1. Using a static mesh reader

 * Run ParaView
 * Load the plugin
 * Open an EnSight file that you know is a static mesh and contains only an unstructured grid
 * Apply: **this will take the same amount of time that if the plugin was not loaded**
 * Change the current timestep: **both the EnSight reader and the surface representation are much faster with the plugin loaded**
 * Add a `SliceWithPlane` filter
 * Apply: **this will take the same amount of time that if the plugin was not loaded**
 * Change the current timestep: **both the EnSight reader and the slice with plane filter are much faster with the plugin loaded**

2. Using any input

 * Run ParaView
 * Load the plugin
 * Open or generate any data that you know is a static mesh and contains only an unstructured grid
 * Add a `ForceStaticMesh` filter,
 * Apply: **this will take some time to create the surface representation**
 * Change the current timestep: **the surface representation is much faster with the plugin loaded**
 * Add a `SliceWithPlane` filter
 * Apply: **this will take the same amount of time that if the plugin was not loaded**
 * Change the current timestep: **the slice with plane filter is much faster with the plugin loaded**

License
-------

This project is distributed under the OSI-approved BSD 3-clauses License.
See the [LICENSE](https://gitlab.kitware.com/paraview/staticmeshplugin/-/blob/master/LICENSE) file for details.
